<?php
/**
 * Profiles controller
 *
 * @author Simon Lyashenko
 * @email simon.lyashenko@gmail.com
 */
class ProfileController extends Zend_Controller_Action {

    /**
     * Profiles list action
     *
     * @return void
     */
    public function indexAction() {
        $type = $this->_getParam('type');
        if (!empty($type)
            && !in_array($type, Profile_Constants::$types)
        ) {
            throw new Zend_Exception('error_notAllowedProfileType');
        }
        $collection = new Profile_Collection();
        $allowedProfiles = Trololo_Registry::getUser()->getAccessibleIds(
            'profile'
        );
        if (!empty($allowedProfiles)) {
            $config = new Profile_Config();
            $config
                ->setIds($allowedProfiles)
            ;
            if (!empty($type)) {
                $config
                    ->setTypes(array($type))
                ;
            }
            $collection = Profile_Service::getCollection($config);
        }
        $this->view->getPage()
            ->setType($type)
            ->setCollection($collection)
        ;
    }

    /**
     * Profile form
     *
     * @return void
     */
    public function addAction() {
        $type = $this->_getParam('type');
        if (!empty($type)
            && !in_array($type, Profile_Constants::$types)
        ) {
            throw new Zend_Exception('error_notAllowedProfileType');
        }
        $profile = new Profile_Object();
        $profile->type = $type;
        $manager = new Profile_Manager();
        $this->view->getPage()
            ->setForm($manager->getForm($profile))
        ;
    }

    /**
     * Action to get AJAX redirect to right profile add/edit form page
     *
     * @throws Zend_Exception
     *
     * @return void
     */
    public function routeAddAction() {
        $type = $this->_getParam('type');
        if (!empty($type)
            && !in_array($type, Profile_Constants::$types)
        ) {
            throw new Zend_Exception('error_notAllowedProfileType');
        }
        $this->_helper->ajaxOutput->success('/profile/add/type/' . $type);
    }

    /**
     * Insert a profile into db
     *
     * @return void
     */
    public function insertAction() {
        $manager = new Profile_Manager();
        try {
            $manager->add($this->_getAllParams());
            $this->_helper->ajaxOutput->success('/profile');
        } catch (Trololo_Exception_FormInvalid $e) {
            $this->_helper->ajaxOutput->validationError($e);
        } catch (Zend_Exception $e) {
            $this->_helper->ajaxOutput->error($e->getMessage());
        }
    }

    /**
     * Get profile edit form page
     *
     * @throws Zend_Exception
     *
     * @return void
     */
    public function editAction() {
        $id = (integer) $this->_getParam('id');
        if (empty($id)) {
            throw new Zend_Exception('error_idNotSpecified');
        }
        $config = new Profile_Config();
        $config->setIds(array($id));
        $profile = Profile_Service::getObject($config);
        $manager = new Profile_Manager();
        $this->view->getPage()
            ->setForm($manager->getForm($profile))
        ;
    }

    /**
     * Update a profile into db
     *
     * @return void
     */
    public function updateAction() {
        $id = (integer) $this->_getParam('id');
        if (empty($id)) {
            $this->_helper->ajaxOutput->error('system_error_idNotSpecified');
        } else {
            $manager = new Profile_Manager();
            try {
                $manager->edit($this->_getAllParams());
                $this->_helper->ajaxOutput->success('/profile');
            } catch (Trololo_Exception_FormInvalid $e) {
                $this->_helper->ajaxOutput->validationError($e);
            } catch (Zend_Exception $e) {
                $this->_helper->ajaxOutput->error($e->getMessage());
            }
        }
    }

    /**
     * Profile delete action
     *
     * @return void
     */
    public function deleteAction() {
        $id = (integer) $this->_getParam('id');
        if (empty($id)) {
            $this->_helper->ajaxOutput->error('system_error_idNotSpecified');
            return;
        }
        $manager = new Profile_Manager();
        try {
            $manager->delete($this->_getAllParams());
            $this->_helper->ajaxOutput->success();
        } catch (Zend_Exception $e) {
            $this->_helper->ajaxOutput->error(
                'error_couldNotDeleteProfile',
                $e->getMessage()
            );
        }
    }

    /**
     * Get remote ids of geo/time/whatever
     *
     * Expects the followng parameters:
     * engine - ad serving engine
     * type - profile type
     * suggestionType - one of, vk only
     * substring - objects are going to be filtered by this substring,
     *     vk and google
     * countryId - id of country where the cities/regions in question
     *     are situated, vk only
     *
     * @return void
     */
    public function getRemoteDefinitionsAction() {
        $engine = $this->_getParam('engine');
        $type = $this->_getParam('type');
        if (!in_array($engine, Trololo_Context_Constants::$engines)
            || !in_array($type, Profile_Constants::$types)
        ) {
            throw new Zend_Exception('internal');
        }
        $manager = new Profile_Manager();
        $response = new Trololo_JsonResponse();
        try {
            $response->data = $manager->getRemoteDefinitions(
                $this->_getAllParams()
            );
            $this->_helper->ajaxOutput->customResponse($response);
        } catch (Exception $e) {
            $this->_helper->ajaxOutput->error($e->getMessage());
        }
    }

}
