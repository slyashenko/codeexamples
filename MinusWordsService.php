<?php
/**
 * Service to generate minus-words for input words data
 * 
 * @required Installed Stem PECL package: http://pecl.php.net/package/stem 
 * to generate word stems: http://en.wikipedia.org/wiki/Word_stem
 * 
 * @author Simon Lyashenko
 * @email simon.lyashenko@gmail.com
 */
class Mediaplan_MinusWordsService {

    /**
     * Generate auto-minuses for input phrases by self comparation
     * 
     * @param array $phrases Array of phrases
     * 
     * @return array
     */
    static public function autoMinus($phrases) {
        // Build array of words and words stems for phrases
        $wordsSet = array();
        $stemsSet = array();
        $minuses = array();
        foreach ($phrases as $index => $phrase) {
            preg_match_all('/\S+/', $phrase, $words);
            $words = $words[0];
            // Doesn't work str_word_count($phrase, 1);
            $stems = array();
            $minus = array();
            foreach ($words as $key => $word) {
                if (strpos($word, '-') !== FALSE) {
                    // Delete minus-sign from word 
                    $clear = substr($word, 1);
                    // Delete word from phrase
                    unset($words[$key]);
                    // Add word to phrase minus-words list
                    $minus[] = $clear;
                    // Add word to stem for phrase
                    $stems[] = stem($clear, STEM_RUSSIAN_UNICODE);
                } else {
                    // Add word to stem for phrase
                    $stems[] = stem($word, STEM_RUSSIAN_UNICODE);
                }
            }
            $phrases[$index] = implode(' ', $words);
            $wordsSet[$index] = $words;
            $stemsSet[$index] = $stems;
            $minuses[$index] = $minus;
        }
        // Genereate minus-words
        for ($i = 0; $i < count($phrases); $i++) {
            for ($j = $i+1; $j < count($phrases); $j++) {
                $cross = array_intersect($stemsSet[$i], $stemsSet[$j]);
                // If phrases hasn't got intersection, they doesn't need minuses
                if (empty($cross)) {
                    continue;
                }
                // Add to minuses of $i-phrase new words from $j-phrase
                $minuses[$i] = array_merge(
                    $minuses[$i],
                    self::getNewWordsByCross(
                        $wordsSet[$j], $stemsSet[$j], $cross
                    )
                );
                // Add to minuses of $j-phrase new words from $i-phrase
                $minuses[$j] = array_merge(
                    $minuses[$j],
                    self::getNewWordsByCross(
                        $wordsSet[$i], $stemsSet[$i], $cross
                    )
                );
            }
        }
        // Build phrases with minuses
        foreach ($minuses as $key => $minus) {
            if (empty($minus)) {
                continue;
            }
            $phrases[$key] = $phrases[$key] 
                . ' -'
                . implode(' -', array_unique($minus))
            ;
        }
        return $phrases;
    }
    
    /**
     * Get words from phrase, which have no stems in intersection
     * 
     * @param array $words Words form phrase
     * @param array $stems Stems of words
     * @param array $cross Intersection of words stems
     * 
     * @return array
     */
    static protected function getNewWordsByCross($words, $stems, $cross) {
        return array_intersect_key(
            $words,
            array_flip(
                array_keys(
                    array_diff($stems, $cross)
                )
            )
        );
    }

}
